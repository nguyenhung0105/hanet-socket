const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
const cors = require('cors')({origin: true});
app.use(cors);

const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

io.on('connection', (socket) => {
  console.log('a user connected');
  socket.on('chat message', msg => {
    io.emit('chat message', msg);
  });
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });
});
app.post('/camera-event', (req, res) => {
    console.log("data",req.body);
    const data = req.body
    io.of("/").emit("checkIn", req.body);
    io.emit('chat message', data.deviceID +":"+data.placeName+" "+ data.personName);
    res.status(200).json({status:"sent"})
});
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});
app.get('/api', (req, res) => {
  const date = new Date();
  const hours = (date.getHours() % 12) + 1;  // London is UTC + 1hr;
  res.json({bongs: 'BONG '.repeat(hours)});
});
var s = server.listen(3000, function () {
   var host = s.address().address
   var port = s.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})
